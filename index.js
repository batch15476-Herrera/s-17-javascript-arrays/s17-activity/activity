// alert('hello')

let student = []

//create an add student function

function addStudent(name) {
    // accept a name of the student
    // add student to the student array
    console.log(`${name} was added to the student's list.`)
    
        student.push(name)
    

}

addStudent('John')
console.log(student)
addStudent('Jane')
console.log(student)
addStudent('Joe')
console.log(student)

//create a countStudent() function

function countStudent() {
    //print the total number of students in the array
    console.log(`The total count of the student array is ${student.length}.`)
}

countStudent()


//create a printStudents() function
function printStudents(){
    //sort and individually print using sort and forEach in array
    student.forEach(function(name){
        student.sort()
        console.log(name)
    })
}

printStudents()

//create a findStudent( function)
function findStudent(words){
    let word = words.toLowerCase()
    
    let result = student.filter(function(name){
        if(name.includes(word)){
            return name
        }

    })
    console.log(result)
    if(result.length == 1){
        console.log(`${result} is an enrolee.`)
        
    }else if(result.length > 1){
        console.log(`${result} are enrolees.`)

    }else if(result.length < 1){
        console.log(`${result} is not an enrollee.`)
        
    }
}
findStudent('J')



    
    